# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="6"
K_NOUSENAME="yes"
K_NOSETEXTRAVERSION="yes"
K_NOUSEPR="yes"
K_SECURITY_UNSUPPORTED="1"
K_BASE_VER="3.14"
K_EXP_GENPATCHES_NOUSE="1"
ETYPE="sources"

inherit kernel-2
detect_version
detect_arch

DESCRIPTION="Asus Transformer specific patches for the ${KV_MAJOR}.${KV_MINOR} kernel tree"
HOMEPAGE="https://www.kernel.org"

SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="binary"

DEPEND="binary? ( sys-kernel/genkernel )"

src_compile() {
		! use binary && return
}
