# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Xorg configuration for Asus T100"
HOMEPAGE="https://github.com/lramage/linux-asus-t100ta"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="|| ( x11-base/xorg-x11 x11-base/xorg-server )"
RDEPEND="${DEPEND}"

src_install() {
	dodir /etc/X11/xorg.conf.d
	cp "${FILESDIR}"/xorg-config" "${ED}"/etc/X11/xorg.conf.d/50-1368x768" || die
}
