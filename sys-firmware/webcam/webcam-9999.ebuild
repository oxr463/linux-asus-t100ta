# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit eutils

DESCRIPTION="Intel Atom Image Signal Processing Driver"
HOMEPAGE="https://github.com/torvalds/linux/tree/master/drivers/staging/media/atomisp"
LICENSE="GPL-2"

SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
