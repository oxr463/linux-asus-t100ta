# Asus Transformer Book T100TA

_Device Specific Portage Overlay for building Gentoo Linux on the Asus Transformer T100_

[![Gentoo][gentoo-badge]][gentoo-url]

**NOTICE: This is a fork of the original repo but with focus on Gentoo Linux.**

The original can be found at: <https://github.com/jfwells/linux-asus-t100ta>

## Installation

    layman -f -a asus-t100ta -o https://gitlab.com/oxr463/linux-asus-t100ta/raw/master/overlay.xml
    
[gentoo-url]: https://www.gentoo.org
[gentoo-badge]: https://www.gentoo.org/assets/img/badges/gentoo-badge2.png
